<?php
/**
 * Created by PhpStorm.
 * User: panik
 * Date: 13.03.2018
 * Time: 13:50
 */

namespace app\controllers\rest;


use yii\rest\ActiveController;

class RestClaimController extends ActiveController
{
    public $modelClass = 'app\models\Claim';

    public function actions()
    {
        return array_merge(parent::actions(), ['update' => null, 'delete' => null, 'index' => null]);
    }


}