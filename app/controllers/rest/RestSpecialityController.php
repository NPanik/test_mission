<?php
/**
 * Created by PhpStorm.
 * User: panik
 * Date: 13.03.2018
 * Time: 13:53
 */

namespace app\controllers\rest;


use yii\rest\ActiveController;

class RestSpecialityController extends ActiveController
{
    public $modelClass = 'app\models\Speciality';

    public function actions()
    {
        return array_merge(parent::actions(), ['create' => null, 'update' => null, 'delete' => null]);
    }
}