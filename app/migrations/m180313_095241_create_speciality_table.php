<?php

use yii\db\Migration;

/**
 * Handles the creation of table `speciality`.
 */
class m180313_095241_create_speciality_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('speciality', [
            'id' => $this->bigPrimaryKey(),
            'name' => $this->string(255)->notNull(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('speciality');
    }
}
