<?php

use yii\db\Migration;

/**
 * Class m180313_133216_i18n_init
 */
class m180313_133216_i18n_init extends Migration
{
    public function up()
    {
        $this->createTable('i18n_source_message', [
            'id' => $this->primaryKey(),
            'category' => $this->string(),
            'message' => $this->text(),
        ]);

        $this->createTable('i18n_message', [
            'id' => $this->integer()->notNull(),
            'language' => $this->string(16)->notNull(),
            'translation' => $this->text(),
        ]);

        $this->addPrimaryKey('pk_message_id_language', 'i18n_message', ['id', 'language']);
        $this->addForeignKey('fk_message_source_message', 'i18n_message', 'id', 'i18n_source_message', 'id', 'CASCADE', 'RESTRICT');
        $this->createIndex('idx_source_message_category', 'i18n_source_message', 'category');
        $this->createIndex('idx_message_language', 'i18n_message', 'language');
    }

    public function down()
    {
        $this->dropForeignKey('fk_message_source_message', 'i18n_message');
        $this->dropTable('i18n_message');
        $this->dropTable('i18n_source_message');
    }
}
