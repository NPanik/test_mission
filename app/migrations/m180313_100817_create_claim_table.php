<?php

use yii\db\Migration;

/**
 * Handles the creation of table `claim`.
 */
class m180313_100817_create_claim_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('claim', [
            'id' => $this->bigPrimaryKey(),
            'name' => $this->string(255)->notNull(),
            'surname' => $this->string(255)->notNull(),
            'patronymic' => $this->string(255),
            'email' => $this->string(255)->notNull(),
            'speciality_id' => $this->bigInteger()->notNull(),
            'degree_id' => $this->bigInteger()->notNull(),
            'problem_description' => $this->text()->defaultValue(''),
            'date' => $this->date()->null(),
            'payed' => $this->boolean()->defaultValue(false)
        ]);

        $this->addForeignKey(
            'fk-claim-speciality_id',
            'claim',
            'speciality_id',
            'speciality',
            'id',
            'CASCADE',
            'CASCADE'
        );

        $this->addForeignKey(
            'fk-claim-degree_id',
            'claim',
            'degree_id',
            'degree',
            'id',
            'CASCADE',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('claim');
    }
}
