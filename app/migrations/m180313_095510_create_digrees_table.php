<?php

use yii\db\Migration;

/**
 * Handles the creation of table `degrees`.
 */
class m180313_095510_create_digrees_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('degree', [
            'id' => $this->bigPrimaryKey(),
            'name' => $this->string(255)->notNull()
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('degree');
    }
}
