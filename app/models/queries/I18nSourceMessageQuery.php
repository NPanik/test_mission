<?php

namespace app\models\queries;

/**
 * This is the ActiveQuery class for [[\app\models\I18nSourceMessage]].
 *
 * @see \app\models\I18nSourceMessage
 */
class I18nSourceMessageQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return \app\models\I18nSourceMessage[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return \app\models\I18nSourceMessage|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
