<?php

namespace app\models\queries;

/**
 * This is the ActiveQuery class for [[\app\models\I18nMessage]].
 *
 * @see \app\models\I18nMessage
 */
class I18nMessageQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return \app\models\I18nMessage[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return \app\models\I18nMessage|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
