<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "speciality".
 *
 * @property int $id
 * @property string $name
 *
 * @property Claim[] $claims
 */
class Speciality extends TranslatableActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'speciality';
    }

    public function translatableAttributes()
    {
        return ['name'];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Speciality name'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClaims()
    {
        return $this->hasMany(Claim::className(), ['speciality_id' => 'id']);
    }

    /**
     * {@inheritdoc}
     * @return \app\models\queries\SpecialityQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\models\queries\SpecialityQuery(get_called_class());
    }
}
