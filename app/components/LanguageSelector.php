<?php
/**
 * Created by PhpStorm.
 * User: panik
 * Date: 13.03.2018
 * Time: 20:36
 */

namespace app\components;
use yii\base\BootstrapInterface;

class LanguageSelector implements BootstrapInterface
{
    public $supportedLanguages = [];

    public function bootstrap($app)
    {
        $preferredLanguage = isset($app->request->headers['Accept-Language']) ? (string)$app->request->headers['Accept-Language'] : null;

        if (empty($preferredLanguage)) {
            $preferredLanguage = $app->request->getPreferredLanguage($this->supportedLanguages);
        }

        $app->language = $preferredLanguage;
    }
}